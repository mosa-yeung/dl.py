import os

# returns a dict with the piece start and end offsets
def get_piece_info(piecename):
    if piecename.split('.')[-2] != 'piece':
        raise Exception('get_piece_info() was provided a filename that is not a piece file')

    offsets = piecename.split('.')[-1]
    start = offsets.split('-')[0]
    end = offsets.split('-')[1]

    return {'name':piecename, 'start':int(start), 'end':int(end)}

# returns a list of all the piece_info for this file, sorted by start offset
def get_pieces_info(dl_name):
    pieces = []
    for file in os.listdir('.'):
        if file.startswith(dl_name + '.piece.'):
            piece = get_piece_info(file)

            sz = os.path.getsize(piece['name'])
            expected_sz = piece['end'] - piece['start'] + 1
            if sz != expected_sz:
                raise Exception('Piece {0} is incomplete [{1}/{2}]'.format(piece['name'], sz, expected_sz)) 
            
            pieces.append(piece)

    pieces.sort(key=lambda piece: piece['start'])

    return pieces

# returns the last offset that we have contiguous data until
def contiguous_until(all_pieces):
    start = 0
    for piece in all_pieces:
        if piece['start'] != start:
            break;

        start = piece['end'] + 1
    
    return start
