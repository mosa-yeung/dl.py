import requests
import io
from requests import Request, Session

## get_headers ##
def get_headers(url):
    r = requests.get(url, stream=True)
    if r.status_code == 404:
        raise Exception('File not found (404)')
    return requests.structures.CaseInsensitiveDict(r.headers)

## accepts_ranges ##
def accepts_ranges(headers):
    if 'Accept-Ranges' in headers:
        return headers['Accept-Ranges'] == 'bytes'
    return False

## file_sz ##
def file_sz(headers):
    if 'content-length' in headers:
        return headers['content-length']
    return 0

## prep_req ##
def prep_req(url, start, end):
    req = Request('GET', url)
    prepped = req.prepare()
    prepped.headers={'Range' : 'bytes={0}-{1}'.format(start, end)}
    return prepped

## dl_one ##
def dl_one(src, dst, start_offset, end_offset, chunk_sz=128*1024):
    # check arguments
    if len(dst) == 0:
        raise Exception('dst not provided to dl_one()')

    # create a session
    s = Session()

    # send request
    prepped_req = prep_req(src, start_offset, end_offset)
    r = s.send(prepped_req, stream=True)

    # write to disk
    with open(dst, "wb+") as f:
        for chunk in r.iter_content(chunk_size=chunk_sz):
            if chunk:
                f.write(chunk)
                f.flush()
    return
