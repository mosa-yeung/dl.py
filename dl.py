import sys
import threading
from net import *
from pieces import *
from urllib.parse import urlparse
from pathlib import PurePath

# check arguments
if len(sys.argv) < 2 :
    sys.exit('usage: python dl.py <file url> [<number of pieces>]')

url = urlparse(sys.argv[1])
headers = get_headers(url.geturl())

pieces_count = 4
if len(sys.argv) > 2:
    pieces_count = int(sys.argv[2])

# check if we can split the file
if False == accepts_ranges(headers):
    raise Exception('server does not support "Accepts-Ranges"')

# decide on file name
name = PurePath(url.path).name
if len(name) == 0:
    name = 'download.dat'
print('File name:', name)

# get file size
sz = int(file_sz(headers))
print('Total size:', sz, 'Bytes')

piece_sz = sz // pieces_count

# start download!
threads = [];
next_offset = 0
for i in range(0, pieces_count):
    start = next_offset
    
    if (i+1 == pieces_count):
      # last piece need to explicitly ask for the end, in case it doesn't divide evenly
      end = sz - 1
    else:
      # get roughly 1/pieces_count of the file
      end = start + piece_sz
      next_offset = end + 1
    t = threading.Thread(target=dl_one, args=[url.geturl(), '{0}.piece.{1}-{2}'.format(name, start, end), start, end])
    t.start()
    threads.append(t)

for t in threads:
    t.join()

# check that we got everything
all_pieces = get_pieces_info(name)
dled_sz = contiguous_until(all_pieces);
if (sz != dled_sz):
    raise Exception('Pieces sum up to {0}. Expected {1}'.format(dled_sz, sz))

# put them back together!
print('Download complete! Recombining pieces...')
with open(name, 'wb') as out_file:
    for piece_info in all_pieces:
        with open(piece_info['name'], 'rb') as in_piece:
            while True:
                buff=in_piece.read(1024)
                if buff:
                    out_file.write(buff)
                else:
                    break
    
# cleanup
for piece_info in all_pieces:
    os.remove(piece_info['name'])
